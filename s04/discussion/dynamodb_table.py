import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.create_table(
        TableName = 'b332-repollo-table',
        KeySchema = [
            {
                'AttributeName': 'Id',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions = [
            {
                'AttributeName': 'Id',
                'AttributeType': 'S'
            }
        ],
        BillingMode = 'PAY_PER_REQUEST'
)

#Print table status
print("Table status:", table.table_status)
