import boto3

# create a DynamoDB client
dynamodb = boto3.client('dynamodb')

# create a new table
table_name = 'b332-repollo-user'
dynamodb.create_table(
    TableName = table_name,
    KeySchema = [
        {
            'AttributeName': 'Id',
            'KeyType': 'HASH'
        }
    ],
    AttributeDefinitions = [
        {
            'AttributeName': 'Id',
            'AttributeType': 'N'
        }
    ],
    BillingMode = 'PAY_PER_REQUEST'
)

# wait for the table to be created
waiter = dynamodb.get_waiter('table_exists')
waiter.wait(TableName = table_name)

# add some items to the table
dynamodb.put_item(
    TableName = table_name,
    Item = {
        'Id': {'N': '1'},
        'Name': {'S': 'Alice'},
        'Age': {'N': '25'},
        'Email': {'S': 'alice@example.com'}
    }
)
dynamodb.put_item(
    TableName = table_name,
    Item = {
        'Id': {'N': '2'},
        'Name': {'S': 'Bob'},
        'Age': {'N': '30'},
        'Email': {'S': 'bob@example.com'}
    }
)

# query the table for items with a certain name
response = dynamodb.query(
    TableName = table_name,
    KeyConditionExpression = 'Id = :Id',
    ExpressionAttributeValues = {
        ':Id': {'N': '2'}
    }
)

# print the query results
for item in response['Items']:
    print(item)
