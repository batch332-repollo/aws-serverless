import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('b332-repollo-table')

response = table.scan(
    FilterExpression = 'Quantity >= :value',
    ExpressionAttributeValues = {
        ':value': '1',
    }
)

items = response['Items']
for item in items:
    print(item)
