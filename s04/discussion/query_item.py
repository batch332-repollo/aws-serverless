import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('b332-repollo-table')
response = table.query(
    KeyConditionExpression = '#pk = :pkValue',
    ExpressionAttributeNames = {
        '#pk': 'Id',
    },
    ExpressionAttributeValues = {
        ':pkValue': '1',
    }
)

items = response['Items']
for item in items:
    print(item)
