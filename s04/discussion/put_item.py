import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('b332-repollo-table')

response = table.put_item(
    Item = {
        'Id': '4',
        'Food': 'Steak',
        'Quantity': '20'
    }
)

# Eliminate attributes that are not included upon updating all elements in the row.
# 'Quantity': '0'

print(response)
