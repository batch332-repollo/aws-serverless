import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('b332-repollo-table')

# This acts the same way as PATCH method API. Only updates the specified attribute.
response = table.update_item(
    Key = {
        'Id': '3'
    },
    UpdateExpression = 'set Quantity = :newValue',
    ExpressionAttributeValues = {
        ':newValue': '20'
    }
)

print(response)
