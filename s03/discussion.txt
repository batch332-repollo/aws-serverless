s03: AWS API Gateway

What is an API?
	- APIs (Application Programming Interface) are interfaces or sets of rules that allow different software applications to communicate with each other. They enable developers to create modular and reusable code that can be integrated into different applications or services.

	- APIs play a crucial role in web development. In web development, APIs are typically used to connect web applications to external systems or services, or to provide a way for different parts of an application to communicate with each other.

How Amazon API Gateway works?
	1. AWS API Gateway is placed right before the Lambda Function.
	2. When a client makes a request to the Amazon API Gateway, the request is received by the Gateway and processed based on the specified method and resource path. 
		- For example, if a client makes a GET request to the endpoint /users, the API Gateway will check if there is a Lambda function configured to handle this request.
	3. If a Lambda function is configured, the Gateway will invoke the function with the details of the request, such as the request body and any query parameters. The Lambda function then processes the request and returns a response.
	4. The response is then sent back to the client through the API Gateway, which may perform additional processing such as caching the response, applying response headers or validating the response before sending it back to the client.
	5. The API Gateway acts as a layer between the client and the backend services, providing a unified interface for all client requests to access different backend services such as AWS Lambda, HTTP endpoints, and more.

Example on how to use Amazon API Gateway
	Let’s get started with using Amazon API Gateway. When we call the HTTP API, the API Gateway routes the request to our Lambda function. Lambda executes the Lambda function and responds to API Gateway. We will then receive a response from API Gateway.

	A. Create a lambda function
		1. In the AWS Management console, navigate to "Lambda" under "compute services".
		2. On the Lambda dashboard, Click on "Create Function" button.
		3. Select "Author from Scratch", and for the "Basic information" type the following:
			- Function name: lambda-b332-lastname-api-gateway
			- Runtime: Node.js 18.x (default)
			- Let other settings to default.

			Note: For creating a new Lambda function, you have the following options:
				- "Author from Scratch": Full control and customization of your Lambda function code.
				- "Use a Blueprint": Quick start with pre-configured templates for common use cases.
				- "Container Image": Packaging your function and its dependencies as a Docker container for greater flexibility in language and dependency management.
		4. Click the "Create function" to save.
		5. A lambda function is now created with sample code from AWS in node.js.

	B. Create Amazon API Gateway (HTTP API)
		1. Search for "API Gateway" service inside the AWS Management console.
		2. On the API Gateway dashboard, click on "Create API".
		3. Choose the "HTTP API" from the selection by clicking the "Build" button.
			Note: 
				- "HTTP API" is a lightweight and cost-effective option for simple HTTP-based APIs.
				- "REST API" provides more control and features for building comprehensive APIs adhering to REST principles.
				- "WebSocket API" enables real-time, bidirectional communication between clients and servers.
		4. For integrating the created Lambda function into the API that we are about to create click on "Add Integration" button.
		5. Select "Lambda" in the "Integrations dropdown", then select the "lambda function" you just created (lambda-b332-lastname-api-gateway) and finally give a name for your HTTP API.
			Note: API Gateway have 4 steps 
				- "Create API": Specify the backend services that your API will communicate with. This includes the Integrations and API name.
				- "Configure routes": Designate specific routes (resources and methods) for your API, determining which endpoints clients can access. This also set up integrations with backend services or Lambda functions.
				- "Define Stages": Create deployment stages, representing different environments like development, testing, and production.
					- The "$default" stage is automatically created for new HTTP APIs and receives automatic deployments. Custom stages can be added to represent different environments, each with its own settings. Stages allow for controlled management of API versions and configurations.
				- "Review and Create": Verify your configuration choices for the API, routes, and stages, ensuring accuracy. 
		6. Click "Review and Create" button. 
		7. Review the integration and names given.Then click on "Create" button.

	C. Test by invoking the API
		1. Use a web browser to invoke your API.
			- On the created API, you will see a table and click the value under the "Invoke URL" column.
		2. Append the name of your "lambda function" at the end of the invoked url.
			example: https://h9bdvbxica.execute-api.us-west-2.amazonaws.com/lambda-batch-000-lastname-my-api-gateway

			Note: Now, your browser sends a "GET request" to the API that you’ve just created.
		3. Verify your API’s response. You should see the text “Hello from Lambda!” in your browser.

API Gateway, Lambda and S3 Integration

	Part 1:
		1. Create an S3 bucket.
		2. Creating and Uploading HTML Files
		3. Set our S3 bucket permissions to host the static HTML web pages.
		4. View our web page.

	Part 2:
		1. Create a Lambda Function with Python.
		2. Attach an API Gateway as a trigger for the Lambda Function.
		3. Create a new Index.html file with some JavaScript.
		4. Upload and Test our new Website and Lambda Function

Part 1: S3 Bucket Setup
	A. Create a bucket
	1. Go to your AWS Services and search for "S3 Bucket".
	2. Click on the "Create Bucket" button, to proceeed on creating a S3 Bucket.
	3. Inside the Create Bucket we have the following:
		- "General Configuration": This is where we create our bucket name and select where we want to put the bucket.
			Note: "Bucket Names" are considered "Global", which means a bucket name cannot be used more than once anywhere. 
				- For example, if we create a bucket in US-East-1 (N. Virginia) named “test-bucket-01”, we cannot create a bucket in AP-South-1 (Asia Pacific — Mumbai) with the same name.

		- For this session use this name format: bucket-batch-332-lastname-serverless

		- "Block Public Access": This blocks all external access to your bucket, excluding you. The only person that would be able to access this bucket is the bucket owner, which would be you in this case.

		- "Bucket Versioning": This allows you to maintain version variants of all items within your bucket. 
			- For example, if we put a text document in the bucket, and then an updated version of the text document later, we’ll have a version 1 and 2 of that document, as long as we didn’t change the name of the document.

		- "Tags": This allows you to keep track of your buckets. If you have all finance department documents in the bucket, you can tag it for “Finance Dept.”. If you have HR-related documents, then you can tag them for “HR Dept.”.
			- For this creation we don't need to specify any tag because we are currently using the bucket for hosting the frontend.

		- "Default Encryption": This is used if you have to store important documents in a bucket. It will encrypt objects as you upload them to the bucket and will decrypt the objects when you download them from the bucket. 
			Note: For hosting static websites, you don’t want to use this. Set the Default encryption to Disabled.

		- "Advanced Settings": This contains the Object Lock feature that is used to help prevent objects from being deleted or overwritten.
			- This is by default set to Disabled

	4. Click on the "Create bucket" button.

	5. Select your bucket to be redirected to your created bucket overview.

	B. Creating and Uploading HTML Files.
		1. Create an index.html and error.html file in your local machine with the following content.

				- Index.html:

				<!DOCTYPE html>
				<html>
					<head>
						<meta charset="utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>Home</title>
					</head>
					<body>
						<h1>Welcome to the Index Page.</h1>
					</body>
				</html>

				- Error.html

				<!DOCTYPE html>
				<html>
					<head>
						<meta charset="utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>Error</title>
					</head>
					<body>
						<h1>An error has occured!</h1>
						<p>Try again later.</p>
					</body>
				</html>

		2. After creating the two html files, go back to your bucket and navigate to the "Permissions" tab.
		3. Go to "Bucket Policy section" and click "Edit" button.
		4. Paste the code below in the "Edit Bucket Policy" section.
			{
			    "Version": "2012-10-17",
			    "Statement": [
			        {
			            "Sid": "PublicReadGetObject",
			            "Effect": "Allow",
			            "Principal": "*",
			            "Action": "s3:GetObject",
			            "Resource": "arn:aws:s3:::[bucket-name]/*"
			        }
			    ]
			}

			Note: Change the "[bucket-name]" with your bucket name.
		5. Click "Save Changes" button below to apply the changes.
		6. Go back to our Bucket’s "Objects section" and click "Upload" button.
		7. Click on "Add files" button and add the "two html files".
		8. You should see both items inside your bucket.

	C. Set S3 bucket permissions to host the static HTML web pages.
		1. Go to "Properties" section in your Bucket to turn on Static Web Hosting.
		2. On the Properties page, just scroll to the bottom to see the "Static web hosting" section.
		3. On the far right of the section, select the "Edit" button.
		4. Click "Enable" to activate our static website.
		5. In the Index document, type index.html and for the Error document type error.html.
		6. Click "Save changes".

	D. View our web page.
		1. Return to the "Properties" page
		2. Scroll back to the very bottom. You should now see your website is up and running.
		3. Click the link there or just copy it and paste it into a new tab to continue. Your website should now be visible.

Part 2: Lambda and API Gateway setup and Connecting to S3 Bucket
	A. Create a Lambda Function with Python.
		1. Go to your AWS Services and under Compute, select "Lambda".
		2. Click the "Create function" button at the upper right corner, to create a Lambda function.
		3. For this function, supply the following details:
			- Author from scratch
			- Basic Information:
				- Function name: lambda-batch-000-lastname-serverless
				- Runtime: Python 3.9
		4. Click "Create function" button and you will be redirected to created lambda function page. 
			- You will see a notification saying that your lambda function is successfully created.
		5. You’ll notice that our Lambda Function already has sample code in the "Code Source" Tab.
			Note: If you have experience with coding in JSON, you’ll primarily be coding that and your Runtime language together.

				- You should approach Lambda as if you were adding components to your main file. For example:

					- Want to change header text with a button click? Create a Lambda Function.
					- Want to add new account info into DynamoDB after sign-up? Create a Lambda Function.
					- Want a new HTML page to open after clicking a link? Create a Lambda Function.
		6.  Under "Code source", replace the default code and paste the code below instead.

			def lambda_handler(event, context):
			    return {
			        'statusCode': 200,
			        'headers': {
			            "Access-Control-Allow-Origin": "*",
			        },
			        'body': "This text is coming from Lambda!"
			    }

			Code Breakdown:
				"def lambda_handler()": This is the name of the lambda function. You can call it whatever you want, but it’s best just to leave it as is.

				"(event, context)": 
					- "event" represents the event, or trigger, that causes this function to run. 
						- For example, if your Lambda is set to trigger by an upload to S3, then that is the event. 
					- "context" is metadata that AWS provides that is supplied to the handler.
						- For example, it gives data like Status Code, Source IP Address, AWS Region, Event Name (like ObjectCreate:Put), etc.

				"statusCode": We want to return the statusCode back to the context to make sure it matches. 
					- In this case, it's set to 200, indicating a successful response.

				"headers": This dictionary contains "HTTP headers" to be included in the response. 

				"Access-Control-Allow-Origin": This enables CORS (Cross Origin Resource Sharing) for our Lambda Function and responds back to whatever domain (“*”) called it.

				"body": This is what will be returned to whatever called our function.

				In summary, this AWS Lambda function acts as an HTTP endpoint that, when invoked, responds with a 200 OK status code, includes a permissive CORS header, and provides a simple text response in the body. 

	B. Attach an API Gateway as a trigger for the Lambda Function.

		1. In the "Function Overview" section, you will see "Add Trigger" button. Go ahead and click that.
		2. On the "Add trigger" page, Click on the "dropdown menu" and select "API Gateway".
		3. After that, choose "Create a new API". You should see a lot more configuration.
			- API type: Choose "HTTP API"
			- Security: Choose "Open"
				- "open" means that it is accessible to a wide range of clients without any significant restrictions.
				- "Create JWT Authorizer" refers to the process of setting up a JSON Web Token (JWT) authorizer in Amazon Web Services (AWS) API Gateway. 
			- Under "Additional settings" set the following:
				- API Name: Leave as is
				- Deployment Stage: Leave as is.
					- Here, you could give it a stage name like “Prod”, “Dev”, “UAT”, “SIT”, etc. 
				- Cross-Origin Resource Sharing: "Check" this box because we are using “Access-Control-Allow-Origin” header within our code.
				- Enable Detailed Metrics: Leave this uncheck.
		4. Once done on configuration, click "Add" button.
		5. On your lambda page, you should see your newly created trigger.

	C. Add our Lambda function to our static website.
		1. Go back to your created S3 bucket(bucket-batch-332-lastname-serverless). 
		2. Refactor your index.html with some javascript (in your local machine) with the given code below:

			<!DOCTYPE html>
			<html>
				<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<script>
					    function getLambdaAPI(){
					        var xhttp = new XMLHttpRequest();

					        xhttp.onreadystatechange = function(){
					            if(this.readyState == 4 && this.status ==200){
					                document.getElementById("my-demo").innerHTML = this.responseText;
					            }
					        };
					        xhttp.open("GET", "[INSERT API GATEWAY ENDPOINT]", true);
					        xhttp.send();
					    }
					    
					</script>
					<title>Home</title>
				</head>
				<body>
					<!-- <h1>Welcome to the Index Page.</h1> -->
					<div align="center">
					    <br/>
					    <br/>
					    <br/>
					    <br/>
					    <h1><span id="my-demo">This is the new Index Page!</span></h1>
					    <button onclick="getLambdaAPI()">Click Me!</button>
					</div>
				</body>
			</html>

			Javascript Code Breakdown
				1. getLambdaAPI(): This is our JavaScript function.
				2. var xhttp: This is our variable.
				3. XMLHttpRequest(): This allows our code to retrieve data from a URL without having to do a full page refresh.
				4. xhttp.onreadystatechange = function(): This becomes an anonymous function (a function without a name); When readyState attribute changes, this will by activated.
				5. this.readyState == 4: “this” refers to “xhttp.onreadystatechange”; “readyState” refers to the state of the request; “4” means “response received”, otherwise the “request has not yet been sent”.
				6. this.status == 200: This basically means if the request was successful.
				7. document.getElementById(“my-demo”).innerHTML: This gets or sets the HTML or XML markup contained within the element we’re looking for, in this case, whatever element has “id=my-demo”.
				8. this.responseText: Returns the text from the place that we sent the request to.
				9. xhttp.open(“GET”, “[INSERT API GATEWAY ENDPOINT]”, true): “open” initializes a newly created request, or re-initializes an existing one; “GET” is the method being used to retrieve whatever we need; “[INSERT API GATEWAY ENDPOINT]” is where we place our API Gateway endpoint; “true” is used to perform this operation asynchronously.
				10. xhttp.send(): This sends the request to the our intended destination.

		3. Return to the Lambda Function tab and replace "[INSERT API GATEWAY ENDPOINT]" with your API Gateway endpoint.
			- Go to  the "Configuration", and look for the "API Gateway" trigger.
			- Copy the "API endpoint:" value.
	
	D. Upload and Test your new Website and Lambda Function.
		1. Go to your S3 tab and reupload the modified index.html file.
		2. Check your index.html on your Bucket page.
		3. On "Properties" tab, scroll to the very bottom part and under "Static website hosting" section, click or copy and paste the link and you have to see your newly added index.html.
		4. On the hosted page, click the "Click Me!" Button, the text should changed.

